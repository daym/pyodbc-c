
#ifndef _WRAPPER_H_
#define _WRAPPER_H_

#if 0
class XXXX
{
protected:
    PyObject* p;

    // GCC freaks out if these are private, but it doesn't use them (?)
    // XXXX(const XXXX& illegal);
    // void operator=(const XXXX& illegal);

public:
    XXXX(PyObject* _p = 0)
    {
        p = _p;
    }

    ~XXXX()
    {
        Py_XDECREF(p);
    }

    XXXX& operator=(PyObject* pNew)
    {
        Py_XDECREF(p);
        p = pNew;
        return *this;
    }

    bool IsValid() const { return p != 0; }

    bool Attach(PyObject* _p)
    {
        // Returns true if the new pointer is non-zero.

        Py_XDECREF(p);
        p = _p;
        return (_p != 0);
    }

    PyObject* Detach()
    {
        PyObject* pT = p;
        p = 0;
        return pT;
    }

    operator PyObject*() 
    {
        return p;
    }

    PyObject* Get()
    {
        return p;
    }
};

class Tuple
    : public XXXX
{
public:
    
    Tuple(PyObject* _p = 0)
        : XXXX(_p)
    {
    }

    operator PyTupleObject*()
    {
        return (PyTupleObject*)p;
    }

    PyObject*& operator[](int i) 
    {
        I(p != 0);
        return PyTuple_GET_ITEM(p, i);
    }

    Py_ssize_t size() { return p ? PyTuple_GET_SIZE(p) : 0; }
};
#endif

#if 0
#ifdef WINVER
struct RegKey
{
    HKEY hkey;

    RegKey()
    {
        hkey = 0;
    }

    ~RegKey()
    {
        if (hkey != 0)
            RegCloseKey(hkey);
    }

    operator HKEY() { return hkey; }
};
#endif
#endif

#endif // _WRAPPER_H_
