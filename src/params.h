
#ifndef PARAMS_H
#define PARAMS_H

bool Params_init(void);

struct Cursor;

bool PrepareAndBind(struct Cursor* cur, PyObject* pSql, PyObject* params, bool skip_first);
void FreeParameterData(struct Cursor* cur);
void FreeParameterInfo(struct Cursor* cur);

#endif
