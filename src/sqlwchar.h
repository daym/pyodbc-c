
#ifndef _PYODBCSQLWCHAR_H
#define _PYODBCSQLWCHAR_H

typedef SQLWCHAR ODBCCHAR;

enum {
	ODBCCHAR_SIZE = sizeof(ODBCCHAR)
};

struct SQLWChar
{
    // An object designed to convert strings and Unicode objects to SQLWCHAR,
    // hold the temporary buffer, and delete it in the destructor.

    ODBCCHAR* pch;
    Py_ssize_t len;
    bool owns_memory;
};
void sqlwchar_init_globals(void);
void SQLWChar_init_from_PyObject(struct SQLWChar* self, PyObject* o);
void SQLWChar_done(struct SQLWChar* self);
Py_ssize_t SQLWChar_size(struct SQLWChar* self);
ODBCCHAR* SQLWChar_getItem(struct SQLWChar* self, Py_ssize_t i);

/* note: SQLWCHAR != SQLWChar */

// The size of a SQLWCHAR.
extern Py_ssize_t SQLWCHAR_SIZE;

// Allocate a new Unicode object, initialized from the given string.
PyObject* PyUnicode_FromODBCCHAR(const ODBCCHAR* sz, Py_ssize_t cch);

ODBCCHAR* ODBCCHAR_FromUnicode(const Py_UNICODE* pch, Py_ssize_t len);

#endif // _PYODBCSQLWCHAR_H
