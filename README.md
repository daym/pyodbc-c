
# pyodbc

pyodbc makes it easy to connect to databases using ODBC.  It implements the DB
API 2.0 specification, but adds even more convenience.

This library, pyodbc-c, is designed to be stand-alone.
It does not have other dependencies and only uses Python's built-in data types.
It doesn't have C++ as dependency either.

To install, use pip or setuptools::

    pip install pyodbc-c

[Documentation](http://mkleehammer.github.io/pyodbc)
